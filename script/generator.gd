extends Spatial

var idx_seed : int = 0
var elapsed_time = 0
var w = 12

var objects_inst = [
	{
		"chance": 1,
		"pos_y": [0],
		"inst": preload("res://prefab/cactus_b1.tscn"),
	},
	{
		"chance": 1,
		"pos_y": [0],
		"inst": preload("res://prefab/cactus_b2.tscn"),
	},
	{
		"chance": 1,
		"pos_y": [0],
		"inst": preload("res://prefab/cactus_b4.tscn"),
	},

	{
		"chance": .5,
		"pos_y": [0.75, 0.5, 0],
		"inst": preload("res://prefab/pterodactyl.tscn"),
	},
]


func _get_rnd_inst():
	var full_chance = 0
	
	for obj in  objects_inst:
		full_chance += obj.chance
		
	var chance = rand_range(0, full_chance)
	
	var cur_chance = 0
	for obj in  objects_inst:
		cur_chance += obj.chance
		if cur_chance >= chance:
			return obj
	
	return null


func get_idx():
	idx_seed += 1
	return idx_seed


func _create():
	var obj_inst = _get_rnd_inst()
	var obj = obj_inst.inst.instance()
	
	obj.translation = Vector3(w, obj_inst.pos_y[randi() % obj_inst.pos_y.size()], 0)
	obj.name = "%s_%d" % [obj.name, get_idx()]
	add_child(obj)

	
func reset():
	elapsed_time = 0
	_clear()

	
func _clear():
	for c in get_children():
		c.queue_free()


func _ready():
	pass # Replace with function body.


func _get_rnd_delay(percent):
	var left = 0.5
	var delay = 2.0 * (1.0 - clamp(percent, 0.0, 1.0))
	var right = left + delay  
	return rand_range(left, right)


func update_generator(speed, dt, percent):
	elapsed_time -= dt
	if elapsed_time <= 0:
		_create()
		elapsed_time = _get_rnd_delay(percent)
		
	for c in get_children():
		c.translation -= Vector3(speed*dt, 0, 0)
		if c.translation.x < (w * -1):
			c.queue_free()

