extends Area

enum TirexState {
	Idle,
	Run,
	Dead,
	Jump,
	Crouch,
}

export (Color) var boost_color = Color(0, 1, 0, 1)

var state = TirexState.Idle
var boost_time = 0
var ani

func can_jamp():
	return state == TirexState.Run

func change_state(state):
	if self.state == state:
		return
	
	self.state = state
#	print(str(self.state))
	_apply_state()
	
func _apply_state():
	match state:
		TirexState.Idle:
			ani.play("idle")
			
		TirexState.Run:
			ani.play("run")
			$col_shape_1.disabled = false
			$col_shape_2.disabled = true
			
		TirexState.Dead:
			ani.play("dead")
			
		TirexState.Jump:
			ani.play("idle")
			
		TirexState.Crouch:
			ani.play("crouch")
			$col_shape_1.disabled = true
			$col_shape_2.disabled = false


func boost_start(life_time):
	boost_time = life_time
	$spr.self_modulate = boost_color

func _ready():
	ani = get_node("spr/ani")
	_apply_state()

func reset():
	state = TirexState.Idle
	_apply_state()
	
func is_imortal():
	return boost_time > 0
	
func _process(delta):
	if boost_time > 0:
		boost_time -= delta
		
		if boost_time <= 0:
			$spr.self_modulate = Color(1, 1, 1, 1)
	
	
