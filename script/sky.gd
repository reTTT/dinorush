extends Spatial

var cloud_inst = preload("res://prefab/cloud.tscn")
var w = 12
var h = 4

var idx = 0

func _create_cloud(pos):
	var cloud = cloud_inst.instance()
	cloud.translation = pos
	cloud.name = "%s_%d" % [cloud.name, idx]
	add_child(cloud)
	idx += 1

func _create_start():
	var pos_x = -w
	
	while pos_x < w:
		var x = rand_range(0.75, 1.5)
		var pos = Vector3(pos_x + x, rand_range(-h, h), 0)
		_create_cloud(pos)
		pos_x += x

func _ready():
	_create_start()
	pass # Replace with function body.

func _update_sky(speed, dt):
	for c in get_children():
		c.translation += Vector3(speed / 8 * dt * -1, 0, 0)
		if c.translation.x < -w:
			c.translation = Vector3(w, rand_range(-h, h), 0)
