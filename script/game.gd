extends Node

enum GameState {
	Menu,
	Play,
	GameOver
}

var state = GameState.Menu
var tirex_pos
var tirex
var jumped = false

var score = 0
var score_last
var hi_score = 0

class Speed:
	var min_val = 9
	var max_val = 15
	
	func _init(minval, maxval):
		min_val = minval
		max_val = maxval
		
	func calc_speed(score, imortal):
		if imortal:
			return max_val
		
		return min((int(score) / 100) * 50 + min_val, max_val)
		
	func calc_percent(speed):
		return 1.0 / (max_val - min_val) * (speed - min_val)
		
var speed = Speed.new(9, 13)

func add_score(score):
#	print(str(score_last))
#	print(str(score))
	
	score_last = self.score
	self.score += score
	_update_score()


func _update_score():
	if floor(self.score) > floor(score_last):
		var text
		if hi_score > 0:
			text = "HI: %05d %05d" % [int(hi_score), int(self.score)]
		else:
			text = "%05d" % [int(self.score)]
			
		$"ui/score".text = text

func change_state(state):
	if self.state == state:
		return
		
	self.state = state
	_apply_state()
	
func _apply_state():
	
	match state:
		GameState.Menu:
			tirex.change_state(tirex.TirexState.Idle)
			pass
		GameState.Play:
			jumped = false
			tirex.change_state(tirex.TirexState.Run)
			pass
		GameState.GameOver:
			if hi_score < score:
				hi_score = score
			score = 0
			_update_score()
			$tween.stop_all()
			tirex.change_state(tirex.TirexState.Dead)
			$"dlg/game_over".visible = true
			pass

func _ready():
	randomize()
	tirex = $"2d/tirex"
	tirex_pos = tirex.translation
	tirex.connect("area_entered", self, "_on_tirex_colision")
	pass


func _process(delta):
	if state != GameState.Play:
		return
		
	var speed_val = speed.calc_speed(score, tirex.is_imortal())
	var percent = speed.calc_percent(speed_val)
	
	var ground = $"2d/bkg/ground"
	ground.region_rect.position.x += speed_val * delta * 100
	
	$"2d/bkg/generator".update_generator(speed_val, delta, percent)
	$"2d/bkg/cloud"._update_sky(speed_val, delta)
	add_score((speed_val * delta) / 1)
	
func jump():
	if jumped || !tirex.can_jamp():
		return
	
	jumped = true
	var ani_len_half = .3
	var h = 2
	
	$tween.interpolate_callback(tirex, 0, "change_state", tirex.TirexState.Jump)
	$tween.interpolate_method(tirex, "set_translation", tirex.translation, tirex.translation + Vector3(0, h, 0), ani_len_half, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$tween.interpolate_callback(tirex, ani_len_half*2, "change_state", tirex.TirexState.Run)
	$tween.interpolate_method(tirex, "set_translation", tirex.translation + Vector3(0, h, 0), tirex.translation, ani_len_half, Tween.TRANS_LINEAR, Tween.EASE_IN, ani_len_half)
	$tween.interpolate_callback(self, ani_len_half*2, "_on_finish_jump")
	$tween.start()
	
func _on_finish_jump():
	jumped = false


func crouch(enable):
	if jumped:
		return
	
	if enable:
		tirex.change_state(tirex.TirexState.Crouch)
	else:
		tirex.change_state(tirex.TirexState.Run)

func _input(ev):
	if ev is InputEventMouseButton:
		match state:
			GameState.Menu:
				if !ev.is_pressed() && !ev.is_echo():
					change_state(GameState.Play)
			GameState.Play:
				if ev.is_pressed() && ev.button_index == BUTTON_LEFT:
					jump()
	elif ev is InputEventKey:
		match state:
			GameState.Menu:
				if !ev.is_pressed() && !ev.is_echo():
					change_state(GameState.Play)
			GameState.Play:
				if ev.is_pressed() && (ev.scancode ==  KEY_SPACE || ev.scancode == KEY_UP):
					jump()
				elif ev.is_pressed() && ev.scancode == KEY_DOWN && !ev.is_echo():
					crouch(true)
				elif !ev.is_pressed() && ev.scancode == KEY_DOWN && !ev.is_echo():
					crouch(false)


func _on_reset_pressed():
	$"dlg/game_over".visible = false
	$"2d/tirex".translation = tirex_pos
	$"2d/bkg/generator".reset()
	change_state(GameState.Play)

func _on_tirex_colision(area):
#	return
	if area is Boost:
		tirex.boost_start(area.life_time)
		
	elif !tirex.is_imortal():
		change_state(GameState.GameOver)